INSERT INTO user_admins (
    id,
    created_at,
	updated_at,
	deleted_at,
    name,
    type,
    password,
    email,
    token
)
VALUES (
    '550e8400-e29b-41d4-a716-446655440000', -- Example UUID
    CURRENT_TIMESTAMP, -- Use current timestamp for created_at
	NULL,
	NULL,
    'User Admin',
    '',
    '12345',
    'email@email.com',
    NULL
);