package entity

import "github.com/google/uuid"

type Stocks struct {
	BaseEntity
	ProductId   string    `gorm:"column:product_id"`
	ProductName *string   `gorm:"column:product_name"`
	Stock       int       `gorm:"column:stock"`
	WarehouseId uuid.UUID `gorm:"column:warehouse_id"`
	Status      string    `gorm:"column:status"`
}

func (a *Stocks) TableName() string {
	return "stock"
}
