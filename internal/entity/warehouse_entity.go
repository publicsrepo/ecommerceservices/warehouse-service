package entity

type Warehouses struct {
	BaseEntity
	Name          string  `gorm:"column:name"`
	Description   *string `gorm:"column:description;type:text"`
	Capacity      int     `gorm:"column:capacity"`
	Address       string  `gorm:"column:address"`
	City          string  `gorm:"column:city"`
	State         string  `gorm:"column:state"`
	PostalCode    string  `gorm:"column:postalCode"`
	Country       string  `gorm:"column:country"`
	Phone         string  `gorm:"column:phone"`
	Email         string  `gorm:"column:email"`
	WarehouseType string  `gorm:"column:warehouseType"`
	Status        string  `gorm:"column:status"`
}

func (a *Warehouses) TableName() string {
	return "warehouses"
}
