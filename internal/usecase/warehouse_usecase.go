package usecase

import (
	"fmt"
	"time"
	"warehouse-services/internal/entity"
	"warehouse-services/internal/model"
	"warehouse-services/internal/model/payload"
	"warehouse-services/internal/model/transformers"
	"warehouse-services/internal/repository"
	"warehouse-services/pkg/errs"
	"warehouse-services/pkg/helper"
	helpers "warehouse-services/pkg/helper"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

type WarehouseUseCase struct {
	DB                  *gorm.DB
	Log                 *logrus.Logger
	Validate            *validator.Validate
	WarehouseRepository *repository.WarehouseRepository
	Config              *viper.Viper
	//WarehouseProducer   *messaging.WarehouseProducer
}

func NewWarehouseUseCase(db *gorm.DB, logger *logrus.Logger, validate *validator.Validate, Config *viper.Viper, warehouseRepository *repository.WarehouseRepository) *WarehouseUseCase {
	return &WarehouseUseCase{
		DB:                  db,
		Log:                 logger,
		Validate:            validate,
		WarehouseRepository: warehouseRepository,
		Config:              Config,
	}
}

func (c *WarehouseUseCase) CreateWarehouse(fiberCtx *fiber.Ctx, request *model.CreateWarehouseRequest) (*model.GetWarehouseDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()

	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	err := c.Validate.Struct(request)
	if err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	now := helper.GetCurrentTimeWithUTC()
	warehouse := &entity.Warehouses{
		BaseEntity: entity.BaseEntity{
			Id:        uuid.New(),
			CreatedAt: now,
			UpdatedAt: nil,
			DeletedAt: nil,
		},
		Name:          request.Name,
		Description:   request.Description,
		Capacity:      request.Capacity,
		Address:       request.Address,
		City:          request.City,
		State:         request.State,
		PostalCode:    request.PostalCode,
		Country:       request.Country,
		Phone:         request.Phone,
		Email:         request.Email,
		WarehouseType: request.WarehouseType,
	}

	if err := c.WarehouseRepository.Create(tx, warehouse); err != nil {
		desc := fmt.Sprintf("Failed create warehouse to database : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	return transformers.GetWarehouseDetailToResponse(warehouse), nil
}

func (c *WarehouseUseCase) UpdateWarehouse(fiberCtx *fiber.Ctx, request *model.UpdateWarehouseRequest) (*model.GetWarehouseDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	idParsed, errIdParsed := uuid.Parse(request.ID)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", errIdParsed)
		c.Log.WithError(errIdParsed).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	warehouse := new(entity.Warehouses)
	if err := c.WarehouseRepository.FindById(tx, warehouse, idParsed); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusNotFound).
			SetMessage(fiber.StatusNotFound).
			SetDescription(desc)
	}
	now := time.Now()

	warehouse.Name = request.Name
	warehouse.Description = request.Description
	warehouse.Capacity = request.Capacity
	warehouse.Address = request.Address
	warehouse.City = request.City
	warehouse.State = request.State
	warehouse.PostalCode = request.PostalCode
	warehouse.Country = request.Country
	warehouse.Phone = request.Phone
	warehouse.Email = request.Email
	warehouse.WarehouseType = request.WarehouseType
	warehouse.UpdatedAt = &now

	if err := c.WarehouseRepository.Update(tx, warehouse); err != nil {
		desc := fmt.Sprintf("Failed save warehouse : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction: %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	return transformers.GetWarehouseDetailToResponse(warehouse), nil
}

func (c *WarehouseUseCase) DeleteWarehouse(fiberCtx *fiber.Ctx, request *model.DeleteWarehouseRequest) (bool, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	idsUuid, errIdParsed := helpers.ManyStringToUUID(request.Ids)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(*errIdParsed)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	warehouses, err := c.WarehouseRepository.GetListWarehouse(tx, payload.GetListWarehouse{Id: idsUuid})
	if err != nil {
		desc := fmt.Sprintf("Failed find warehouse by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	timeNow := helper.GetCurrentTimeWithUTC()
	for _, u := range warehouses {
		u.DeletedAt = &timeNow
		if err := c.WarehouseRepository.Update(tx, &u); err != nil {
			desc := fmt.Sprintf("Failed update warehouse : %+v", err)
			c.Log.WithError(err).Warnf(desc)
			return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
				SetMessage(fiber.StatusInternalServerError).
				SetDescription(desc)
		}
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)

	}

	// event := transformers.WarehouseToEvent(warehouse)
	// c.Log.Info("Publishing warehouse created event")
	// if err := c.WarehouseProducer.Send(event); err != nil {
	// 	c.Log.Warnf("Failed publish warehouse created event : %+v", err)
	// 	return nil, fiber.ErrInternalServerError
	// }

	return true, nil
}

func (c *WarehouseUseCase) GetDetailWarehouse(fiberCtx *fiber.Ctx, request *model.GetWarehouseDetailRequest) (*model.GetWarehouseDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	uuidParsed, errIdParsed := uuid.Parse(request.ID)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(errIdParsed)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	warehouse := entity.Warehouses{}
	if err := c.WarehouseRepository.FindById(tx, &warehouse, uuidParsed); err != nil {
		desc := fmt.Sprintf("Failed find warehouse by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	return transformers.GetWarehouseDetailToResponse(&warehouse), nil
}

func (c *WarehouseUseCase) GetListWarehouse(fiberCtx *fiber.Ctx, request *model.GetListParamsRequest) ([]model.GetWarehouseDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	warehouses, err := c.WarehouseRepository.GetListWarehouse(tx, payload.GetListWarehouse{
		GetListParamsRequest: *request,
	})
	if err != nil {
		desc := fmt.Sprintf("Failed find warehouse by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	return transformers.GetWarehouseListToResponse(warehouses), nil
}
