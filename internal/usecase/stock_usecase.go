package usecase

import (
	"fmt"
	"time"
	"warehouse-services/internal/entity"
	"warehouse-services/internal/model"
	"warehouse-services/internal/model/payload"
	"warehouse-services/internal/model/transformers"
	"warehouse-services/internal/repository"
	"warehouse-services/pkg/errs"
	"warehouse-services/pkg/helper"
	helpers "warehouse-services/pkg/helper"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

type StockUseCase struct {
	DB                  *gorm.DB
	Log                 *logrus.Logger
	Validate            *validator.Validate
	StockRepository     *repository.StockRepository
	WarehouseRepository *repository.WarehouseRepository
	Config              *viper.Viper
	//StockProducer   *messaging.StockProducer
}

func NewStockUseCase(db *gorm.DB, logger *logrus.Logger, validate *validator.Validate, Config *viper.Viper, stockRepository *repository.StockRepository, warehouseRepository *repository.WarehouseRepository) *StockUseCase {
	return &StockUseCase{
		DB:                  db,
		Log:                 logger,
		Validate:            validate,
		StockRepository:     stockRepository,
		WarehouseRepository: warehouseRepository,
		Config:              Config,
	}
}

func (c *StockUseCase) CreateStock(fiberCtx *fiber.Ctx, request *model.CreateStockRequest) (*model.GetStockDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()

	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	err := c.Validate.Struct(request)
	if err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	now := helper.GetCurrentTimeWithUTC()
	stock := &entity.Stocks{
		BaseEntity: entity.BaseEntity{
			Id:        uuid.New(),
			CreatedAt: now,
			UpdatedAt: nil,
			DeletedAt: nil,
		},
		ProductId:   request.ProductId,
		ProductName: nil,
		Stock:       request.Stock,
		WarehouseId: request.WarehouseId,
	}

	if err := c.StockRepository.Create(tx, stock); err != nil {
		desc := fmt.Sprintf("Failed create stock to database : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	return transformers.GetStockDetailToResponse(stock), nil
}

func (c *StockUseCase) UpdateStock(fiberCtx *fiber.Ctx, request *model.UpdateStockRequest) (*model.GetStockDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	idParsed, errIdParsed := uuid.Parse(request.ID)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", errIdParsed)
		c.Log.WithError(errIdParsed).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	stock := new(entity.Stocks)
	if err := c.StockRepository.FindById(tx, stock, idParsed); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusNotFound).
			SetMessage(fiber.StatusNotFound).
			SetDescription(desc)
	}
	now := time.Now()

	stock.ProductId = request.ProductId
	stock.Stock = request.Stock
	stock.WarehouseId = request.WarehouseId
	stock.UpdatedAt = &now

	if err := c.StockRepository.Update(tx, stock); err != nil {
		desc := fmt.Sprintf("Failed save stock : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction: %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	return transformers.GetStockDetailToResponse(stock), nil
}

func (c *StockUseCase) DeleteStock(fiberCtx *fiber.Ctx, request *model.DeleteStockRequest) (bool, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	idsUuid, errIdParsed := helpers.ManyStringToUUID(request.Ids)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(*errIdParsed)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	stocks, err := c.StockRepository.GetListStock(tx, payload.GetListStock{Id: idsUuid})
	if err != nil {
		desc := fmt.Sprintf("Failed find stock by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	timeNow := helper.GetCurrentTimeWithUTC()
	for _, u := range stocks {
		u.DeletedAt = &timeNow
		if err := c.StockRepository.Update(tx, &u); err != nil {
			desc := fmt.Sprintf("Failed update stock : %+v", err)
			c.Log.WithError(err).Warnf(desc)
			return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
				SetMessage(fiber.StatusInternalServerError).
				SetDescription(desc)
		}
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return false, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)

	}

	// event := transformers.StockToEvent(stock)
	// c.Log.Info("Publishing stock created event")
	// if err := c.StockProducer.Send(event); err != nil {
	// 	c.Log.Warnf("Failed publish stock created event : %+v", err)
	// 	return nil, fiber.ErrInternalServerError
	// }

	return true, nil
}

func (c *StockUseCase) GetDetailStock(fiberCtx *fiber.Ctx, request *model.GetStockDetailRequest) (*model.GetStockDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	uuidParsed, errIdParsed := uuid.Parse(request.ID)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(errIdParsed)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	stock := entity.Stocks{}
	if err := c.StockRepository.FindById(tx, &stock, uuidParsed); err != nil {
		desc := fmt.Sprintf("Failed find stock by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	return transformers.GetStockDetailToResponse(&stock), nil
}

func (c *StockUseCase) GetListStock(fiberCtx *fiber.Ctx, request *model.GetListParamsRequest) ([]model.GetStockDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	stocks, err := c.StockRepository.GetListStock(tx, payload.GetListStock{
		GetListParamsRequest: *request,
	})
	if err != nil {
		desc := fmt.Sprintf("Failed find stock by id : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}

	return transformers.GetStockListToResponse(stocks), nil
}

func (c *StockUseCase) TransferWarehouseStock(fiberCtx *fiber.Ctx, request *model.TransferProductRequest) ([]model.GetStockDetailResponse, *errs.Errs) {
	ctx := fiberCtx.UserContext()
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		desc := fmt.Sprintf("Invalid request body : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusBadRequest).
			SetMessage(fiber.StatusBadRequest).
			SetDescription(desc)
	}
	idNewParsed, errIdParsed := uuid.Parse(request.WarehouseNewId)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(errIdParsed).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}
	idOldParsed, errIdParsed := uuid.Parse(request.WarehouseOldId)
	if errIdParsed != nil {
		desc := fmt.Sprintf("Invalid UUID : %+v", errIdParsed)
		c.Log.WithError(errIdParsed).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	warehouseOld := new(entity.Warehouses)
	warehouseNew := new(entity.Warehouses)
	if err := c.WarehouseRepository.FindById(tx, warehouseOld, idOldParsed); err != nil {
		desc := fmt.Sprintf("Warehouse Not Found : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusNotFound).
			SetMessage(fiber.StatusNotFound).
			SetDescription(desc)
	}

	if err := c.WarehouseRepository.FindById(tx, warehouseNew, idNewParsed); err != nil {
		desc := fmt.Sprintf("Warehouse Not Found : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusNotFound).
			SetMessage(fiber.StatusNotFound).
			SetDescription(desc)
	}

	pids := []uuid.UUID{}
	for _, products := range request.ProductId {
		idProductParsed, errIdproductParsed := uuid.Parse(products.Id)
		if errIdproductParsed != nil {
			desc := fmt.Sprintf("Invalid UUID : %+v", errIdproductParsed)
			c.Log.WithError(errIdproductParsed).Warnf(desc)
			return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
				SetMessage(fiber.StatusInternalServerError).
				SetDescription(desc)
		}
		pids = append(pids, idProductParsed)

	}

	payloadFilter := payload.GetListStock{
		ProductId:   pids,
		WarehouseId: []uuid.UUID{idNewParsed},
	}
	stocks, err := c.StockRepository.GetListStock(tx, payloadFilter)
	if err != nil {
		desc := fmt.Sprintf("Get List Stock Failed : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	if err := c.StockRepository.Updates(tx, stocks, payloadFilter); err != nil {
		desc := fmt.Sprintf("Failed save stock : %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	if err := tx.Commit().Error; err != nil {
		desc := fmt.Sprintf("Failed commit transaction: %+v", err)
		c.Log.WithError(err).Warnf(desc)
		return nil, errs.NewErrContext(fiberCtx, fiber.StatusInternalServerError).
			SetMessage(fiber.StatusInternalServerError).
			SetDescription(desc)
	}

	return transformers.GetStockListToResponse(stocks), nil
}
