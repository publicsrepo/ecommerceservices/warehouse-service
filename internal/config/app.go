package config

import (
	"warehouse-services/internal/delivery/http"
	"warehouse-services/internal/delivery/http/middleware"
	"warehouse-services/internal/delivery/http/route"
	"warehouse-services/internal/repository"
	"warehouse-services/internal/usecase"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

type BootstrapConfig struct {
	DB       *gorm.DB
	App      *fiber.App
	Log      *logrus.Logger
	Validate *validator.Validate
	Config   *viper.Viper
}

func Bootstrap(config *BootstrapConfig) {
	// setup repositories
	warehouseRepository := repository.NewWarehouseRepository(config.Log)

	// setup use cases
	warehouseUseCase := usecase.NewWarehouseUseCase(config.DB, config.Log, config.Validate, config.Config, warehouseRepository)

	// setup controller
	warehouseController := http.NewWarehouseController(warehouseUseCase, config.Log)

	// setup middleware
	authMiddleware := middleware.NewAuth(warehouseUseCase)
	routeConfig := route.RouteConfig{
		App:                 config.App,
		WarehouseController: warehouseController,
		AuthMiddleware:      authMiddleware,
	}
	routeConfig.Setup()
}
