package repository

import (
	"warehouse-services/internal/entity"
	"warehouse-services/internal/model/payload"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type WarehouseRepository struct {
	Repository[entity.Warehouses]
	Log *logrus.Logger
}

func NewWarehouseRepository(log *logrus.Logger) *WarehouseRepository {
	return &WarehouseRepository{
		Log: log,
	}
}

func (r *WarehouseRepository) GetListWarehouse(db *gorm.DB, payloads payload.GetListWarehouse) ([]entity.Warehouses, error) {
	Warehouse := []entity.Warehouses{}
	q := db.Where("deleted_at is NULL")

	if len(payloads.Id) > 0 {
		q = q.Where("id in (?)", payloads.Id)
	}
	if len(payloads.Category) > 0 {
		q = q.Where("category in (?)", payloads.Category)
	}
	if len(payloads.ShopId) > 0 {
		q = q.Where("shop_id in (?)", payloads.ShopId)
	}
	err := q.Find(&Warehouse).Error
	if err != nil {
		return nil, err
	}
	return Warehouse, nil
}
