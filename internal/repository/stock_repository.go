package repository

import (
	"warehouse-services/internal/entity"
	"warehouse-services/internal/model/payload"

	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type StockRepository struct {
	Repository[entity.Stocks]
	Log *logrus.Logger
}

func NewStockRepository(log *logrus.Logger) *StockRepository {
	return &StockRepository{
		Log: log,
	}
}

func (r *StockRepository) GetListStock(db *gorm.DB, payloads payload.GetListStock) ([]entity.Stocks, error) {
	Stock := []entity.Stocks{}
	q := db.Where("deleted_at is NULL")

	if len(payloads.Id) > 0 {
		q = q.Where("id in (?)", payloads.Id)
	}

	if len(payloads.WarehouseId) > 0 {
		q = q.Where("warehouse_id in (?)", payloads.WarehouseId)
	}

	if len(payloads.ProductId) > 0 {
		q = q.Where("product_id in (?)", payloads.ProductId)
	}
	err := q.Find(&Stock).Error
	if err != nil {
		return nil, err
	}
	return Stock, nil
}

func (r *StockRepository) Updates(db *gorm.DB, data []entity.Stocks, payloads payload.GetListStock) error {
	result := []entity.Stocks{}
	q := db.Model(&result).Where("deleted_at IS NULL")

	if len(payloads.WarehouseId) > 0 {
		q = q.Where("warehouse_id in (?)", payloads.WarehouseId)
	}

	if len(payloads.ProductId) > 0 {
		q = q.Where("product_id in (?)", payloads.ProductId)
	}

	return q.Updates(data).Error
}
