package http

import (
	"strconv"
	"warehouse-services/internal/delivery/http/middleware"
	"warehouse-services/internal/model"
	"warehouse-services/internal/usecase"
	"warehouse-services/pkg/errs"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type WarehouseController struct {
	UseCaseWarehouse *usecase.WarehouseUseCase
	Log              *logrus.Logger
	Config           *viper.Viper
}

func NewWarehouseController(UseCaseWarehouse *usecase.WarehouseUseCase, log *logrus.Logger) *WarehouseController {
	return &WarehouseController{
		Log:              log,
		UseCaseWarehouse: UseCaseWarehouse,
	}
}

func (c *WarehouseController) CreateWarehouse(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*model.GetWarehouseDetailResponse]{
		Status: "FAILED",
	}
	isActive, _, errVerifyToken := middleware.VerifyConsoleToken(ctx, c.Config)
	if !isActive && errVerifyToken != nil {
		c.Log.Warnf("Failed to parse request body : %+v", errVerifyToken)
		finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusUnauthorized).SetDescription("invalid or Incomplete Access Token")
		return ctx.JSON(finalResult)
	}
	request := new(model.CreateWarehouseRequest)
	if err := ctx.BodyParser(request); err != nil {
		c.Log.Warnf("Failed to parse request body : %+v", err)
		return fiber.ErrBadRequest
	}

	response, err := c.UseCaseWarehouse.CreateWarehouse(ctx, request)
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Data = response
	finalResult.Status = "SUCCESS"
	return ctx.JSON(finalResult)
}

func (c *WarehouseController) UpdateWarehouse(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*model.GetWarehouseDetailResponse]{Status: "FAILED"}
	isActive, _, errVerifyToken := middleware.VerifyConsoleToken(ctx, c.Config)
	if !isActive && errVerifyToken != nil {
		c.Log.Warnf("Failed to parse request body : %+v", errVerifyToken)
		finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusUnauthorized).SetDescription("invalid or Incomplete Access Token")
		return ctx.JSON(finalResult)
	}
	request := new(model.UpdateWarehouseRequest)
	if err := ctx.BodyParser(request); err != nil {
		c.Log.Warnf("Failed to parse request body : %+v", err)
		return fiber.ErrBadRequest
	}
	response, err := c.UseCaseWarehouse.UpdateWarehouse(ctx, request)
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Status = "SUCCESS"
	finalResult.Data = response
	return ctx.JSON(finalResult)
}

func (c *WarehouseController) DeleteWarehouse(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[bool]{Status: "FAILED"}
	isActive, _, errVerifyToken := middleware.VerifyConsoleToken(ctx, c.Config)
	if !isActive && errVerifyToken != nil {
		c.Log.Warnf("Failed to parse request body : %+v", errVerifyToken)
		finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusUnauthorized).SetDescription("invalid or Incomplete Access Token")
		return ctx.JSON(finalResult)
	}

	request := new(model.DeleteWarehouseRequest)
	if err := ctx.BodyParser(request); err != nil {
		c.Log.Warnf("Failed to parse request body : %+v", err)
		return fiber.ErrBadRequest
	}

	response, err := c.UseCaseWarehouse.DeleteWarehouse(ctx, request)
	if response && err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Status = "SUCCESS"
	finalResult.Data = response
	return ctx.JSON(finalResult)
}

func (c *WarehouseController) GetListWarehouse(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*[]model.GetWarehouseDetailResponse]{Status: "FAILED"}

	isActive, _, errVerifyToken := middleware.VerifyConsoleToken(ctx, c.Config)
	if !isActive && errVerifyToken != nil {
		c.Log.Warnf("Failed to parse request body : %+v", errVerifyToken)
		finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusUnauthorized).SetDescription("invalid or Incomplete Access Token")
		return ctx.JSON(finalResult)
	}

	var (
		limitFilter  *int = nil
		offsetFilter *int = nil
	)
	limit := ctx.Query("limit")
	offset := ctx.Query("offset")

	if len(limit) > 0 {
		limitStr, errlimit := strconv.Atoi(limit)
		if errlimit != nil {
			c.Log.WithError(errlimit)
			finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusBadRequest).SetDescription("Invalid Limit Params")
			return ctx.JSON(finalResult)
		}
		limitFilter = &limitStr
	}
	if len(offset) > 0 {
		offsetStr, erroffset := strconv.Atoi(offset)
		if erroffset != nil {
			c.Log.WithError(erroffset)
			finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusBadRequest).SetDescription("Invalid Offset Params")
			return ctx.JSON(finalResult)
		}
		offsetFilter = &offsetStr
	}

	orderBy := ctx.Query("orderBy")
	order := ctx.Query("order")
	search := ctx.Query("search")

	response, err := c.UseCaseWarehouse.GetListWarehouse(ctx, &model.GetListParamsRequest{
		Search:  search,
		Order:   order,
		OrderBy: orderBy,
		Limit:   limitFilter,
		Offset:  offsetFilter,
	})
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}

	finalResult.Status = "SUCCESS"
	finalResult.Data = &response
	return ctx.JSON(finalResult)
}

func (c *WarehouseController) GetDetailWarehouse(ctx *fiber.Ctx) error {
	finalResult := model.WebResponse[*model.GetWarehouseDetailResponse]{Status: "FAILED"}
	isActive, _, errVerifyToken := middleware.VerifyConsoleToken(ctx, c.Config)
	if !isActive && errVerifyToken != nil {
		c.Log.Warnf("Failed to parse request body : %+v", errVerifyToken)
		finalResult.Errors = errs.NewErrContext(ctx, fiber.StatusUnauthorized).SetDescription("invalid or Incomplete Access Token")
		return ctx.JSON(finalResult)
	}

	userId := ctx.Params("warehouse_id")
	response, err := c.UseCaseWarehouse.GetDetailWarehouse(ctx, &model.GetWarehouseDetailRequest{ID: userId})
	if err != nil {
		finalResult.Errors = err
		return ctx.JSON(finalResult)
	}
	finalResult.Status = "SUCCESS"
	finalResult.Data = response
	return ctx.JSON(finalResult)
}
