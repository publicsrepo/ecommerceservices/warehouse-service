package route

import (
	"warehouse-services/internal/delivery/http"

	"github.com/gofiber/fiber/v2"
)

type RouteConfig struct {
	App                 *fiber.App
	WarehouseController *http.WarehouseController
	StockController     *http.StockController
	AuthMiddleware      fiber.Handler
}

func (c *RouteConfig) Setup() {
	c.SetupConsoleRoute()
}

func (c *RouteConfig) SetupConsoleRoute() {
	c.App.Use(c.AuthMiddleware)

	c.App.Put("/api/v1/warehouse-management/transfer", c.StockController.TransferProduct)

	c.App.Post("/api/v1/warehouse-management/stock", c.StockController.CreateStock)
	c.App.Put("/api/v1/warehouse-management/stock", c.StockController.UpdateStock)
	c.App.Delete("/api/v1/warehouse-management/stock", c.StockController.DeleteStock)
	c.App.Get("/api/v1/warehouse-management/stocks", c.StockController.GetListStock)
	c.App.Get("/api/v1/warehouse-management/stock/:stock_id", c.StockController.GetDetailStock)

	c.App.Post("/api/v1/warehouse-management/warehouse", c.WarehouseController.CreateWarehouse)
	c.App.Put("/api/v1/warehouse-management/warehouse", c.WarehouseController.UpdateWarehouse)
	c.App.Delete("/api/v1/warehouse-management/warehouse", c.WarehouseController.DeleteWarehouse)
	c.App.Get("/api/v1/warehouse-management/warehouses", c.WarehouseController.GetListWarehouse)
	c.App.Get("/api/v1/warehouse-management/warehouse/:warehouse_id", c.WarehouseController.GetDetailWarehouse)

	//Activated Deactivated Warehouse
	// c.App.Post("/api/v1/warehouse-management/warehouse/deactive", c.WarehouseController.Activated)
	// c.App.Post("/api/v1/warehouse-management/warehouse/activate", c.WarehouseController.Deactivated)

	//TODO : new endpoint for checkin checkout inventory
	// c.App.Post("/api/v1/warehouse-management/stock/checkin", c.StockController.CheckinStock)
	// c.App.Put("/api/v1/warehouse-management/stock/checkout", c.StockController.CheckoutStock)
}
