package constant

var (
	TypeAdminCode   = 1
	TypeUserCode    = 2
	TypeAdminString = "admin"
	TypeUserString  = "user"
)

var TypeCodeToString = map[int]string{
	1: TypeAdminString,
	2: TypeUserString,
}

var TypeStringToCode = map[string]int{
	TypeAdminString: 2,
	TypeUserString:  1,
}
