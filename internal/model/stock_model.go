package model

import "github.com/google/uuid"

// response
type GetStockDetailResponse struct {
	BaseResponse
	ProductId   string    `json:"product_id"`
	Stock       int       `json:"stock"`
	WarehouseId uuid.UUID `json:"warehouse_id"`
}

// request
type CreateStockRequest struct {
	ProductId   string    `json:"product_id" validate:"required"`
	Stock       int       `json:"stock" validate:"required"`
	WarehouseId uuid.UUID `json:"warehouse_id" validate:"required"`
}

type UpdateStockRequest struct {
	ID          string    `json:"id" validate:"required"`
	ProductId   string    `json:"product_id" validate:"required"`
	Stock       int       `json:"stock" validate:"required"`
	WarehouseId uuid.UUID `json:"warehouse_id" validate:"required"`
}

type TransferProductRequest struct {
	WarehouseOldId string            `json:"warehouse_old_id" validate:"required"`
	WarehouseNewId string            `json:"warehouse_new_id" validate:"required"`
	ProductId      []ProductTransfer `json:"product_id" validate:"required,min=1"`
}

type ProductTransfer struct {
	Id    string `json:"product_id" validate:"required"`
	Stock int    `json:"stock" validate:"required,numeric"`
}
type DeleteStockRequest struct {
	Ids []string `json:"warehouse_ids" validate:"required"`
}

type GetStockDetailRequest struct {
	ID string `json:"warehouse_ids" validate:"required,max=100"`
}
