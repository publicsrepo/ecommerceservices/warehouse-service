package transformers

import (
	"warehouse-services/internal/entity"
	"warehouse-services/internal/model"
)

func GetStockDetailToResponse(stock *entity.Stocks) *model.GetStockDetailResponse {
	result := model.GetStockDetailResponse{}
	if stock == nil {
		return nil
	}

	result.BaseResponse = model.BaseResponse{
		ID:        stock.Id.String(),
		CreatedAt: stock.CreatedAt,
		UpdatedAt: *stock.UpdatedAt,
	}

	result.ProductId = stock.ProductId
	result.Stock = stock.Stock
	result.WarehouseId = stock.WarehouseId

	return &result
}

func GetStockListToResponse(stock []entity.Stocks) []model.GetStockDetailResponse {
	finalResult := []model.GetStockDetailResponse{}

	for _, s := range stock {
		finalResult = append(finalResult, model.GetStockDetailResponse{
			BaseResponse: model.BaseResponse{
				ID:        s.Id.String(),
				CreatedAt: s.CreatedAt,
				UpdatedAt: *s.UpdatedAt,
			},
			ProductId:   s.ProductId,
			Stock:       s.Stock,
			WarehouseId: s.WarehouseId,
		})
	}
	return finalResult
}
