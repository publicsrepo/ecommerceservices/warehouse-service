package transformers

import (
	"warehouse-services/internal/entity"
	"warehouse-services/internal/model"
)

func GetWarehouseDetailToResponse(warehouse *entity.Warehouses) *model.GetWarehouseDetailResponse {
	result := model.GetWarehouseDetailResponse{}
	if warehouse == nil {
		return nil
	}

	result.BaseResponse = model.BaseResponse{
		ID:        warehouse.Id.String(),
		CreatedAt: warehouse.CreatedAt,
		UpdatedAt: *warehouse.UpdatedAt,
	}

	result.Name = warehouse.Name
	result.Description = warehouse.Description
	result.Capacity = warehouse.Capacity
	result.Address = warehouse.Address
	result.City = warehouse.City
	result.State = warehouse.State
	result.PostalCode = warehouse.PostalCode
	result.Country = warehouse.Country
	result.Phone = warehouse.Phone
	result.Email = warehouse.Email
	result.WarehouseType = warehouse.WarehouseType

	return &result
}

func GetWarehouseListToResponse(warehouse []entity.Warehouses) []model.GetWarehouseDetailResponse {
	finalResult := []model.GetWarehouseDetailResponse{}

	for _, w := range warehouse {
		finalResult = append(finalResult, model.GetWarehouseDetailResponse{
			BaseResponse: model.BaseResponse{
				ID:        w.Id.String(),
				CreatedAt: w.CreatedAt,
				UpdatedAt: *w.UpdatedAt,
			},
			Name:        w.Name,
			Description: w.Description,
			Capacity:    w.Capacity,
			Address:     w.Address,
			City:        w.City,
			State:       w.State,
			PostalCode:  w.PostalCode,
			Country:     w.Country,
			Phone:       w.Phone,
			Email:       w.Email,
		})
	}
	return finalResult
}
