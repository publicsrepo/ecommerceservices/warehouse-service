package model

// response
type GetWarehouseDetailResponse struct {
	BaseResponse
	Name          string  `json:"name"`
	Description   *string `json:"description"`
	Capacity      int     `json:"capacity"`
	Address       string  `json:"address"`
	City          string  `json:"city"`
	State         string  `json:"state"`
	PostalCode    string  `json:"postalCode"`
	Country       string  `json:"country"`
	Phone         string  `json:"phone"`
	Email         string  `json:"email"`
	WarehouseType string  `json:"warehouseType"`
}

// request
type CreateWarehouseRequest struct {
	Name          string  `json:"name" validate:"required"`
	Description   *string `json:"description" validate:"required"`
	Capacity      int     `json:"capacity" validate:"required,numeric"`
	Address       string  `json:"address" validate:"required"`
	City          string  `json:"city" validate:"required"`
	State         string  `json:"state" validate:"required"`
	PostalCode    string  `json:"postalCode" validate:"required"`
	Country       string  `json:"country" validate:"required"`
	Phone         string  `json:"phone" validate:"required"`
	Email         string  `json:"email" validate:"required"`
	WarehouseType string  `json:"warehouseType" validate:"required"`
}

type UpdateWarehouseRequest struct {
	ID            string  `json:"id" validate:"required"`
	Name          string  `json:"name" validate:"required"`
	Description   *string `json:"description" validate:"required"`
	Capacity      int     `json:"capacity" validate:"required,numeric"`
	Address       string  `json:"address" validate:"required"`
	City          string  `json:"city" validate:"required"`
	State         string  `json:"state" validate:"required"`
	PostalCode    string  `json:"postalCode" validate:"required"`
	Country       string  `json:"country" validate:"required"`
	Phone         string  `json:"phone" validate:"required"`
	Email         string  `json:"email" validate:"required"`
	WarehouseType string  `json:"warehouseType" validate:"required"`
}

type DeleteWarehouseRequest struct {
	Ids []string `json:"warehouse_ids" validate:"required"`
}

type GetWarehouseDetailRequest struct {
	ID string `json:"warehouse_ids" validate:"required,max=100"`
}

type ActivateWarehouseRequest struct {
	Ids []string `json:"warehouse_ids" validate:"required"`
}

type DectivateWarehouseRequest struct {
	Ids []string `json:"warehouse_ids" validate:"required"`
}
