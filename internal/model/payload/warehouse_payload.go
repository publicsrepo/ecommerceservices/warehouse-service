package payload

import (
	"warehouse-services/internal/model"

	"github.com/google/uuid"
)

type GetListWarehouse struct {
	Id       []uuid.UUID
	Category []string
	ShopId   []uuid.UUID
	model.GetListParamsRequest
}
