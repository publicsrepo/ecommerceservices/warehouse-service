package payload

import (
	"warehouse-services/internal/model"

	"github.com/google/uuid"
)

type GetListStock struct {
	Id          []uuid.UUID
	WarehouseId []uuid.UUID
	ProductId   []uuid.UUID
	model.GetListParamsRequest
}
